import { Injectable } from '@nestjs/common';
import { DbService } from './db.service';

@Injectable()
export class AppService {
  constructor(
    public readonly dbService: DbService,
  ) {
  }
  getHello(): string {
    return 'Hello World!';
  }

  getData(): Promise<DataRowType[]> {
    return this.dbService.getData();
  }
  
  saveData(postDataAny: DataRowType[]): Promise<boolean> {
    return this.dbService.saveData(postDataAny);
  }


}
