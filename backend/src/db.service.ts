
import { Injectable } from '@nestjs/common';
import * as alasql from 'alasql';

@Injectable()
export class DbService {

    private alasql;

    constructor() {
        this.alasql = alasql;
        this.alasql("CREATE TABLE data (CLINIC_NO STRING,BARCODE STRING,PATIENT_ID STRING,PATIENT_NAME STRING,DOB STRING,GENDER STRING,COLLECTIONDATE STRING,COLLECTIONTIME STRING,TESTCODE STRING,TESTNAME STRING,RESULT STRING,UNIT STRING,REFRANGELOW STRING,REFRANGEHIGH STRING,NOTE STRING,NONSPECREFS STRING)");
    }

    async getData(): Promise<DataRowType[]> {
        const data = this.alasql("SELECT * FROM data");
        data.forEach((dataElement,index)=>{
            dataElement.ID = index.toString(10);
        })
        return data;
    }

    async saveData(postDataAny:DataRowType[]): Promise<boolean> {
        alasql.tables.data.data = postDataAny;
        return true;
    }


}


