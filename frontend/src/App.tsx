import { ReactNode } from 'react';
import './App.css';
import Upload from "./pages/Upload";
import Home from "./pages/Home";
import List from "./pages/List";
import Frame from "./components/Frame";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

import {
  QueryClient,
  QueryClientProvider,
} from '@tanstack/react-query'

const queryClient = new QueryClient()
const wrapperFactory = (children: ReactNode) => {
  return <QueryClientProvider client={queryClient}><Frame>{children}</Frame></QueryClientProvider>
}
const router = createBrowserRouter([
  {
    path: "/",
    element: wrapperFactory(<Home />),
  },
  {
    path: "list",
    element: wrapperFactory(<List />),
  },
  {
    path: "upload",
    element: wrapperFactory(<Upload />),
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
