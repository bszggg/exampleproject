import { DataRowType } from "../types/DataType"

export const convertFieldsToDataRowObject = (fields: string[]): DataRowType => {
  return {
    CLINIC_NO: fields[0],
    BARCODE: fields[1],
    PATIENT_ID: fields[2],
    PATIENT_NAME: fields[3],
    DOB: fields[4],
    GENDER: fields[5],
    COLLECTIONDATE: fields[6],
    COLLECTIONTIME: fields[7],
    TESTCODE: fields[8],
    TESTNAME: fields[9],
    RESULT: fields[10],
    UNIT: fields[11],
    REFRANGELOW: fields[12],
    REFRANGEHIGH: fields[13],
    NOTE: fields[14],
    NONSPECREFS: fields[15],
  }
}