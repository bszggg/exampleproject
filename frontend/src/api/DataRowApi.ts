import axios from 'axios'
import { Data } from '../types/DataType'

export const daveData = async (fileData:Data): Promise<Data> => {
    const { data } = await axios.post('http://localhost:3333/savedata', fileData)
    return data
}

export const getDataRows = async (): Promise<Data> => {
    const { data } = await axios.get(
        'http://localhost:3333/getdata',
    )
    return data
}