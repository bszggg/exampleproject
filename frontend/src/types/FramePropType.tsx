import { ReactNode } from 'react';

export type FramePropType = {
  children: ReactNode;
};
