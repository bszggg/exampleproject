import styled from 'styled-components'

export const StyledFileInput = styled.input`
  color: white;
  padding: 16px;
  color: rgba(0, 0, 0, 0.87);
  border: 1px solid rgb(196,196,196);
  border-radius: 4px;
`