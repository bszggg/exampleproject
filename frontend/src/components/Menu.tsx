import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { StyledLink } from "./StyledLink";

const Menu = () => {
  return (
    <>
      <AppBar position="static">
        <Toolbar variant="dense">
          <Typography variant="h6" component="div">
            <StyledLink to="/"> Home</StyledLink>
          </Typography>
          <Typography variant="h6" color="inherit" component="div">
            <StyledLink to="/upload"> Upload</StyledLink>
          </Typography>
          <Typography variant="h6" color="inherit" component="div">
            <StyledLink to="/list"> List</StyledLink>
          </Typography>
        </Toolbar>
      </AppBar>
    </>
  )
}
export default Menu;