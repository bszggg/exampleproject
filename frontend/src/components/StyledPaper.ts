import Paper from '@mui/material/Paper'
import styled from 'styled-components'

export const StyledPaper = styled(Paper)`
  margin:8px;
`