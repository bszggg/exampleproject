import styled from 'styled-components'

export const StyledTitle = styled.h1`
  color: black;
  margin:0 10px;
`