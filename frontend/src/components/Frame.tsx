import Menu from './Menu'
import { FramePropType } from '../types/FramePropType'

const Frame = (props: FramePropType) => {
  const children = props.children;

  return (
    <>
      <div><Menu /></div>
      <div>{children}</div>
    </>
  )
}

export default Frame