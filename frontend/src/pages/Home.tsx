import { StyledTitle } from '../components/StyledTitle';
import logo from './../logo.svg';

const Home = () => {
  return (
    <>
      <StyledTitle>Home</StyledTitle>
      <img src={logo} className="App-logo" alt="logo" />
    </>
  )
}

export default Home