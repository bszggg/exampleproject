import {
  useQuery,
} from '@tanstack/react-query'
import { getDataRows } from '../api/DataRowApi';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { StyledTitle } from '../components/StyledTitle';

const columns: GridColDef[] = [
  { field: 'CLINIC_NO', headerName: 'CLINIC_NO', width: 150 },
  { field: 'BARCODE', headerName: 'BARCODE', width: 150 },
  { field: 'PATIENT_ID', headerName: 'PATIENT_ID', width: 150 },
  { field: 'PATIENT_NAME', headerName: 'PATIENT_NAME', width: 150 },
  { field: 'DOB', headerName: 'DOB', width: 150 },
  { field: 'GENDER', headerName: 'GENDER', width: 150 },
  { field: 'COLLECTIONDATE', headerName: 'COLLECTIONDATE', width: 150 },
  { field: 'COLLECTIONTIME', headerName: 'COLLECTIONTIME', width: 150 },
  { field: 'TESTCODE', headerName: 'TESTCODE', width: 150 },
  { field: 'TESTNAME', headerName: 'TESTNAME', width: 150 },
  { field: 'RESULT', headerName: 'RESULT', width: 150 },
  { field: 'UNIT', headerName: 'UNIT', width: 150 },
  { field: 'REFRANGELOW', headerName: 'REFRANGELOW', width: 150 },
  { field: 'REFRANGEHIGH', headerName: 'REFRANGEHIGH', width: 150 },
  { field: 'NOTE', headerName: 'NOTE', width: 150 },
  { field: 'NONSPECREFS', headerName: 'NONSPECREFS', width: 650 },
];

const List = () => {
  const { isPending, isError, error, data } = useQuery({
    queryKey: ['listData'],
    queryFn: getDataRows,
  })
  if (isPending) {
    return <div>Loading...</div>
  }
  if (isError) {
    return <div>Error: {error.message}</div>
  }
  return (
    <div>
      <StyledTitle>List</StyledTitle>
      <div style={{ height: '100%', width: '100%' }}>
        <DataGrid rows={data} columns={columns}
          getRowId={(row) => row.BARCODE}
        />
      </div>
    </div>
  )


}
export default List;