import React, { useCallback, useState } from 'react'
import { useForm, SubmitHandler } from "react-hook-form"
import { UploadFormInput } from '../types/UploadFormInput'
import { Data } from '../types/DataType'
import { convertFieldsToDataRowObject } from '../helper/convertFieldsToDataRowObject'
import Button from '@mui/material/Button';
import { StyledTitle } from '../components/StyledTitle'
import { StyledFileInput } from '../components/StyledFileInput'
import { StyledPaper } from '../components/StyledPaper'
import Alert from '@mui/material/Alert'
import { useMutation } from '@tanstack/react-query'
import { daveData } from '../api/DataRowApi'

const keys = "CLINIC_NO|BARCODE|PATIENT_ID|PATIENT_NAME|DOB|GENDER|COLLECTIONDATE|COLLECTIONTIME|TESTCODE|TESTNAME|RESULT|UNIT|REFRANGELOW|REFRANGEHIGH|NOTE|NONSPECREFS"

const Upload = () => {
  const [fileError, setFileError] = useState(false); // it needs because of the custom validator function (validate key on prop object of register) doesn't works on file input 
  const [fileData, setFileData] = useState<Data>([]);
  const { register, handleSubmit } = useForm<UploadFormInput>()

  const mutation = useMutation({
    mutationFn: (fileData: Data) => {
      return daveData(fileData)
    },
  })

  const onSubmit: SubmitHandler<UploadFormInput> = () => {
    if (fileError || !fileData.length) return;
    mutation.mutate(fileData);
  }

  const onLoad = useCallback((e: ProgressEvent<FileReader>) => {
    const data: Data = [];
    const contents: string = e.target?.result?.toString() || "";
    const lines = contents.split(/[\r\n]+/g);
    const fileError = !lines[3] || lines[3] !== keys;
    setFileError(fileError);
    if (fileError) return;
    lines.forEach((line, e) => {
      if (e > 3) {
        const fields = line.split("|");
        data.push(convertFieldsToDataRowObject(fields));
      }
    })
    setFileData(data);
  }, [setFileError, setFileData])

  const onChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    const file = (event?.target?.files) ? event.target.files[0] : void 0;
    if (!file) {
      return;
    }

    var reader = new FileReader();
    reader.onload = onLoad;
    reader.readAsText(file);
  }, [onLoad]);

  return (
    <>
      <StyledTitle>Upload</StyledTitle>
      <StyledPaper>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div><StyledFileInput type="file" {
            ...register("file", {
              onChange,
            })} />
            {fileError && <Alert severity="error">This fileinput requires a well structured file</Alert>}</div>
          <div>
            <Button variant="contained" type="submit">submit</Button></div>

          {mutation.isPending ? (
            <Alert severity="info">Processing...</Alert>
          ) : null}
          {mutation.isError ? (
            <Alert severity="error">{mutation.error.message}</Alert>
          ) : null}
          {mutation.isSuccess ? <Alert severity="success">File prcessed</Alert> : null}

        </form>
      </StyledPaper>
    </>
  )

}

export default Upload;