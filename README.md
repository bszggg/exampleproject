# ExampleProject

This is an example project for file upload to a backend, and see the result in frontend.

## Getting started

Please see the readme files inside the folders too. 

To run, please clone the project, install node js and npm to the machine, and inside the folders, type:

```
npm install
```

..and after that:

Frontend: `npm start`
Backend: `npm run start:dev`

## Authors and acknowledgment
Bszggg

## License
GPL

## Project status
Temporarily stopped
